## Simple crawler package for making request, getting response and load response.text as lxml.html or JSON type.

### Instalation: ````pip install aioscraper````.
#### Recomend to use virtual environment.

### The main command to work is ````scraper````. For more information see ````scraper --help````. 
### Basic usage:
* work in interactive shell mode:
    * ````scraper shell -u 'http://example.com' -m 'POST' -d 'value1=1&value2=2'````:
    * after getting and processing response switch to Python interactive console;
* use separate module with Spider class:
    * first step is to call ````scraper makeproject -P spiders````, it make directory called 'spiders' in directory where called ````scraper makeproject spiders````;
    * implement your own scraper inside newly created directory, you can find simple example there;
    * ````scraper crawl -p 'spiders.your_spider_module.YourSpiderClass'```` where 'spiders' is directory you made in previous step, 'your_spider_module' and 'YourSpiderClass' is your file and class respectively;
    * processing and sent back results from scraper to standart output for now.

### Simple example of spider file:
````
from aioscraper.base_scraper import BasicScraper


class TestSpider(BasicScraper):
    # URL you want to scrape
    url = "http://example.com/?param1=value1&param2=value2"
    # HTTP request method
    method = "GET"
    # POST data if request method is POST
    data = "value1&param2=value2"

    def parse(self, content):
        # This method must be implemented in user class.
        # Simple prints the content of loaded and processed page.
        # Could be in lxml or Python dict().
        # You could implement here what you want.
        print(content)
````

### Planned:
* implement callback functions for multiple request, e.g. find all needed links from start URL and then pass this links to specified
    callback for processing;
* save results to JSON/CSV file;
* implement request Headers and possibility to configure it in settings module;
* implement possibility to load spider class by passing only spider
    name;
* implement pipelines to process each parsed Item.

#### See:
##### scraper --help

