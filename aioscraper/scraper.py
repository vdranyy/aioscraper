import argparse
import asyncio
import os
import sys

from aioscraper.base_scraper import BasicScraper
from aioscraper.utils import load_class, make_spider_dir
from aioscraper.web_session import create_session
from aioscraper.logger import setup_logger


# Argparse setup
parser = argparse.ArgumentParser(description="Make request to provided URL and load response in lxml.html tree or json.")
parser.add_argument("mode", type=str, choices=["shell", "crawl", "makeproject"],
                    help="Running in shell mode or from file, 'makeproject' is for "
                    "creating directory for Spider files.")
parser.add_argument("-u", "--url", type=str,
                    help="A URL string to be loaded and parsed.")
parser.add_argument("-d", "--data", type=str,
                    help="POST data in format 'key=value&key=value'.")
parser.add_argument("-m", "--method", type=str, default="GET",
                    help="Request method. Defaults to GET.")
parser.add_argument("-p", "--path", type=str,
                    help="Path to Spider file. Use only if 'crawl' mode is used."
                    "Format is 'somedir.spiderfile.SpiderClass'.")
parser.add_argument("-P", "--project", type=str, default="spiders",
                    help="Name to create directory for spider files. Use only for "
                    " user defined spiders. Location where Scraper is "
                    "loocing for spider classes. Need to be run before "
                    "'crawl' mode is used.")
parser.add_argument("-l", "--logfile", type=str,
                    help="Logging file. Specify only name, e.g."
                    "'my_log_file'. Default location in '$HOME/.aioscraper'. "

                    "You can specify location by setting 'AIOSCRAPER' env variable.")
# Parsing arguments
args = vars(parser.parse_args())
request_url = args.get("url")
method = args.get("method")
data = args.get("data")
mode = args.get("mode")
path = args.get("path")
log_filename = args.get("logfile") or "aioscraper"
log_filename = log_filename + ".log"
project = args.get("project")

if mode == "crawl" and path is None:
    raise Exception("Need to provide --path to Spider class. See scraper.py --help for more information.")
    sys.exit(-1)


def main():
    sys.path.append(os.getcwd())

    loop = asyncio.get_event_loop()
    logger = setup_logger(log_filename)
    session = create_session

    if mode == "shell":
        worker = BasicScraper(
            url=request_url,
            method=method,
            data=data,
            logger=logger,
            loop=loop,
            session=session,
            interactive=True,
        )
    elif mode == "crawl":
        worker = load_class(path, logger)
        if not worker:
            sys.exit()
        worker = worker(
            logger=logger,
            loop=loop,
            session=session,
        )
    else:
        make_spider_dir(project, logger)
        sys.exit()
    try:
        loop.run_until_complete(worker.start())
    except KeyboardInterrupt:
        loop.stop()
    finally:
        loop.close()


if __name__ == "__main__":
    main()
