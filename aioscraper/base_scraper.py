import asyncio
import code
import json
import lxml.html as html
import sys
from lxml.etree import ParserError


class BasicScraper:
    """Base class for all Spiders.
    All user defined class must be inherit from this.
    User must define these class variables:
        url:str - a requested URL
        method:str - HTTP method (GET, POST)
    If you need to made POST request then you need to define a
    class variable:
        data:str - 'key=value&key=value'

    User defined class must implement 'parse' method.
    """

    url = None
    method = None
    data = None

    def __init__(self, **kwargs):
        """Parameters:
            url - str, URL;
            method - str, HTTP method [GET, POST];
            session - aiohttp.ClientSession instance;
            loop - event loop;
            logger - logging.getLogger().
        Optional parameters:
            data - str, POST data (key=value&key=value).
        """

        if not self.url:
            self.url = kwargs.get("url")
        if not self.method:
            self.method = kwargs.get("method")
        if not self.data:
            self.data = kwargs.get("data")
        self._logger = kwargs.get("logger")
        self._loop = kwargs.get("loop")
        self.session = kwargs.get("session")
        self.interactive = kwargs.get("interactive") or False

    async def init_session(self):
        """Coroutine for init client session.
        """

        self.session = await self.session()
        self._logger.info("Initialize ClientSession.")

    async def crawl(self, url=None):
        """Main coroutine for crawling.
        Accepts positional parameter 'url'.
        If runs in 'shell' mode then switch to interactive
        interpreter, or process 'parse' method.
        """

        url = url if url else self.url
        content =  await self._get_content(url)

        content, _format = self.get_tree(content)
        if content is not None:
            self._logger.info("Successfuly load response content in {} "
                                "format".format(_format))
            if self.interactive:
                code.interact(banner="\n[Scraper Interactive Interpreter]\n", local=locals())
            else:
                self.parse(content)
        else:
            self._logger.info("Unable to process response content...\n"
                                "Exiting...")
            await asyncio.sleep(0.1)

    async def _get_content(self, url):
        """Coroutine to make request and get response.
        Accepts positional parameter 'url', and returns
        tuple of content itself and content_type.
        HTTP method takes from self.method.
        """

        self._logger.info("URL for parsing is : {}".format(url))
        async with self.session as session:
            async with getattr(
                session,
                self.method.lower())(
                    url=url,
                    data=self.data) as response:

                self._logger.info("GET {} from {}".format(response.status, url))
                return await self.read_content(response)

    async def read_content(self, response):
        content_type = response.content_type
        content = await response.text()
        return (content, content_type)

    def get_tree(self, content):
        """Method to load response.text as lxml.html or
        regular Python dict() base on response.content_type.
        Returns lxml.html/dict() or None.
        """

        content, content_type = content
        if "text/html" in content_type:
            try:
                return (html.fromstring(content), "HTML")
            except ParserError as e:
                self._logger.info("Can't load response content as HTML...")
        elif "application/json" in content_type:
            try:
                return (json.loads(content), "JSON")
            except json.decoder.JSONDecodeError:
                self._logger.info("Can't load response content as JSON...")
        self._logger.error("Can't process response content.\nUnfixed issue...\n")
        return (None, None)

    def parse(self, content):
        """All user defined classes must implement
        this method. Method accepts one positional argument
        'content' which is processed response text, and loaded
        as lxml or regular Python dict() (loaded from JSON).
        In interactive mode you have this variable available
        if no errors occures.
        In 'crawl' mode (when you implement your own spider) the
        'parse' method could return whatever you want.
        For example:
            1) return data to stdout;
            2) write needed you functionality (pipelines for example,
            or storing data to DB).
        """

        raise NotImplementedError

    async def start(self):
        """Coroutine for invoke session initialization
        and run 'crawl' method.
        """

        await self.init_session()
        await self.crawl()

