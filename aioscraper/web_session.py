import aiohttp


async def create_session():
    """Simple ClientSession coroutine, for passing to
    specified Spider. Coroutine returns a aiohttp.ClientSession().
    """
    return aiohttp.ClientSession()
