import logging
import os


# Logger setup
def setup_logger(filename):
    """Setup logger.
    Function accepts parameters:
        filename - str, default 'pyurlopener.log'.
    User can specify the log file path by setting the
    "PYURLOPENER" environment variable.
    For example:
        export AIOSCRAPER=/home/user/somedir/logs
    """
    base_dir = exists_dir()
    filename = "/".join([base_dir, filename])

    _logger = logging.getLogger("aioscraper")
    _logger.setLevel(logging.INFO)

    _formatter = logging.Formatter("[%(asctime)s] : %(levelname)s : %(name)s : %(message)s")

    _file_handler = logging.FileHandler(filename)
    _file_handler.setLevel(logging.ERROR)
    _file_handler.setFormatter(_formatter)

    _stream_handler = logging.StreamHandler()
    _stream_handler.setFormatter(_formatter)

    _logger.addHandler(_file_handler)
    _logger.addHandler(_stream_handler)
    return _logger


def exists_dir():
    base_path = os.environ.get("AIOSCRAPER")
    if base_path:
        path = base_path
    else:
        base_path = os.environ.get("HOME")
        path = base_path + "/.aioscraper"
    if not os.path.exists(path):
        os.makedirs(path)
    return path

